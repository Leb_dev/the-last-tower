package dev.cyberhatter.thelasttower;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class MovingGameObject extends GameObject {

    Vector2 headingTo;

    MovingGameObject(float x, float y, int width, int height, float toX, float toY, float speed) {
        super(x, y, width, height);
        headingTo = new Vector2(toX - x, toY - y).setLength(speed);
    }

    void update(float delta) {
        position.add(new Vector2(headingTo.x * delta, headingTo.y * delta));
    }

    boolean outOfBounds(Stage stage) {
        return position.x < 0 || position.y < 0 || position.x > stage.getWidth() || position.y > stage.getHeight();
    }

}

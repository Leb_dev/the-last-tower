package dev.cyberhatter.thelasttower;

class Bullet extends MovingGameObject {

    boolean destroyed = false;
    float damage;

    Bullet(float x, float y, int width, int height, float toX, float toY, float speed, float damage) {
        super(x, y, width, height, toX, toY, speed);
        if (x == toX && y == toY) {
            destroyed = true;
        }
        this.damage = damage;
    }

    void decreaseDamage(float decrement) {
        if (decrement >= damage) {
            destroyed = true;
        }
        else {
            damage -= decrement;
        }
    }

}
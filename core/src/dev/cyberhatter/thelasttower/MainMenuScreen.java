package dev.cyberhatter.thelasttower;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

public class MainMenuScreen implements Screen {

    private Stage stage;
    private Label pointsLabel;
    Array<Label> levelLabels = new Array<Label>();
    Array<Label> costLabels = new Array<Label>();
    Array<TextButton> upgradeTextButtons = new Array<TextButton>();
    TheLastTower game;

    MainMenuScreen(final TheLastTower game) {
        this.game = game;
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();
        table.setFillParent(true);
        Table upperTable = new Table();
        Table lowerTable = new Table();

        stage.addActor(table);
        table.add(upperTable).expand();
        table.row();
        table.add(lowerTable).expand().top();

        final Label gameNameLabel = new Label("The last tower", game.skin);
        final TextButton startButton = new TextButton("Click here to start!", game.skin);
        startButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game));
            }
        });

        upperTable.add(gameNameLabel).pad(10);
        upperTable.row();
        upperTable.add(startButton).pad(10);

        pointsLabel = new Label("You have " + game.points + " points to spend", game.skin);
        Label upgradeName = new Label("Upgrade",game.skin);
        Label upgradeCost = new Label("Cost", game.skin);
        Label levelLabel = new Label("Lvl", game.skin);

        lowerTable.columnDefaults(0).width(250);
        lowerTable.add(pointsLabel);
        lowerTable.row();
        lowerTable.add(upgradeName).pad(8);
        lowerTable.add(upgradeCost).pad(8);
        lowerTable.add(levelLabel).pad(8);
        lowerTable.row();

        for (final TheLastTower.Upgrade upgrade : game.upgrades) {
            upgradeName = new Label(upgrade.name, game.skin, game.SKIN_SMALL);
            upgradeCost = new Label(upgrade.getCostRounded() + "", game.skin);
            levelLabel = new Label(upgrade.level + "",game.skin);
            TextButton textButton = new TextButton("+", game.skin);
            textButton.setVisible(upgrade.isAvailable());
            textButton.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    if (upgrade.isAvailable()) {
                        upgrade.levelUp(true);
                        updatePoints(game);
                    }
                }
            });

            costLabels.add(upgradeCost);
            levelLabels.add(levelLabel);
            upgradeTextButtons.add(textButton);

            lowerTable.add(upgradeName);
            lowerTable.add(upgradeCost);
            lowerTable.add(levelLabel);
            lowerTable.add(textButton);
            lowerTable.row();
        }
    }

    private void updatePoints(TheLastTower game) {
        pointsLabel.setText("You have " + game.points + " points to spend");
        for (int i = 0; i < costLabels.size; i++) {
            costLabels.get(i).setText(game.upgrades[i].getCostRounded() + "");
        }
        for (int i = 0; i < levelLabels.size; i++) {
            levelLabels.get(i).setText(game.upgrades[i].level + "");
        }
        for (int i = 0; i < upgradeTextButtons.size; i++) {
            upgradeTextButtons.get(i).setVisible(game.upgrades[i].isAvailable());
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }

        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

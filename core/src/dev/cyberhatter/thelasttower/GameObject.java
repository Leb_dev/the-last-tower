package dev.cyberhatter.thelasttower;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class GameObject {

    Vector2 position;
    private int width;
    private int height;

    GameObject(float x, float y, int width, int height) {
        position = new Vector2(x, y);
        this.width = width;
        this.height = height;
    }

    public Rectangle getRectangle() {
        return new Rectangle(position.x - width / 2, position.y - height / 2, width, height);
    }

}

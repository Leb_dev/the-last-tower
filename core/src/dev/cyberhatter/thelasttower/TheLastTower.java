package dev.cyberhatter.thelasttower;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class TheLastTower extends Game {

    SpriteBatch batch;
    private Music music;
    final int defaultWidth = 800;
    final int defaultHeight = 480;
    public Skin skin;

    class Upgrade {

        String name;
        float cost;
        float costMultiplier;
        float multiplier;
        float value;
        int level;

        Upgrade(String name, float cost, float costMultiplier, float multiplier, float value) {
            this.name = name;
            this.cost = cost;
            this.costMultiplier = costMultiplier;
            this.multiplier = multiplier;
            this.value = value;
            level = 1;
        }

        boolean isAvailable() {
            return points >= getCostRounded();
        }

        float getCostRounded() {
            return Math.round(cost * 10) / 10f;
        }

        public void levelUp(boolean decreasePoints) {
            level++;
            if (decreasePoints) {
                points -= getCostRounded();
                points = Math.round(points * 10) / 10f;
            }
            value *= multiplier;
            cost *= costMultiplier;
        }

        void setLevel(int level) {
            while (this.level < level) {
                levelUp(false);
            }
        }
    }

    final int BULLET_DELAY = 0;
    final int BULLET_SPEED = 1;
    final int BULLET_DAMAGE = 2;
    final int ENEMIES_SPAWN_RATE_GROWTH = 3;
    Upgrade[] upgrades = new Upgrade[]{
            new Upgrade(
                    "Fire rate",
                    15f,
                    2.0f,
                    0.9f,
                    800000000f
            ),
            new Upgrade(
                    "Bullet speed",
                    20f,
                    1.4f,
                    1.2f,
                    20
            ),
            new Upgrade(
                    "Bullet damage",
                    150f,
                    1.1f,
                    1.5f,
                    100
            ),
            new Upgrade(
                    "Enemy spawn rate growth",
                    100f,
                    1.1f,
                    0.9f,
                    0.01f
            )
    };
    float points = 0;

    private final String PREFERENCES_NAME = "gamePreferences";
    private final String POINTS = "points";
    private Preferences preferences;

    private Preferences getPreferences() {
        if (preferences == null) {
            preferences = Gdx.app.getPreferences(PREFERENCES_NAME);
        }
        return preferences;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();

        setSkin();

        setMusic();

        loadGame();

        setScreen(new MainMenuScreen(this));
    }

    private final String SKIN_DEFAULT = "default";
    public final String SKIN_SMALL = "small";
    private void setSkin() {
        skin = new Skin();

        // Generate a 1x1 white texture and store it in the skin named "white".
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Clumsy3d.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 24;
        BitmapFont font = generator.generateFont(parameter);
        skin.add(SKIN_DEFAULT, font);
        parameter.size = 18;
        font = generator.generateFont(parameter);
        generator.dispose();
        skin.add(SKIN_SMALL, font);

        // Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
        textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
        textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
        textButtonStyle.font = skin.getFont(SKIN_DEFAULT);
        skin.add(SKIN_DEFAULT, textButtonStyle);

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = skin.getFont(SKIN_DEFAULT);
        skin.add(SKIN_DEFAULT, labelStyle);
        labelStyle = new Label.LabelStyle();
        labelStyle.font = skin.getFont(SKIN_SMALL);
        skin.add(SKIN_SMALL, labelStyle);
    }

    private void setMusic() {
        music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
        music.setLooping(true);
        music.setVolume(0.4f);
        music.play();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        saveGame();
        super.dispose();
        batch.dispose();
        music.dispose();
        skin.dispose();
    }

    private void saveGame() {
        getPreferences().clear();
        for (Upgrade upgrade : upgrades) {
            getPreferences().putInteger(upgrade.name, upgrade.level);
        }
        getPreferences().putFloat(POINTS, points);
        getPreferences().flush();
    }

    private void loadGame() {
        points = getPreferences().getFloat(POINTS, 0);
        for (Upgrade upgrade : upgrades) {
            upgrade.setLevel(getPreferences().getInteger(upgrade.name, 1));
        }
    }

    void deleteSave() {
        points = 0;
        for (Upgrade upgrade : upgrades) {
            upgrade.setLevel(1);
        }
        getPreferences().clear();
    }

}
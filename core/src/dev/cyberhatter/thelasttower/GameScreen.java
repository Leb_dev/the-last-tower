package dev.cyberhatter.thelasttower;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;
import java.util.Random;


public class GameScreen implements Screen {

    private float enemySpawnDelay = 500000000f;
    private float enemySpeed = 20;
    private float enemySpeedMultiplier = 1.005f;

    private TheLastTower game;
    private Player player;
    private Array<Bullet> bullets = new Array<Bullet>();
    private Array<Enemy> enemies = new Array<Enemy>();
    private Random random = new Random();
    private Texture enemyTexture = new Texture("enemy.png");
    private Texture bulletTexture = new Texture("bullet.png");
    private Texture playerTexture = new Texture("player.png");
    private Texture upgradeTexture = new Texture("upgrade.png");
    private float lastBulletCreatedTime = 0;
    private float lastEnemyCreatedTime = 0;
    private Stage stage;
    private Label pointsLabel;
    private ImageButton upgradeButton;
    private boolean pause = false;

    GameScreen(TheLastTower game) {
        this.game = game;

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        addUI();

        player = new Player(game.defaultWidth / 2, game.defaultHeight / 2, 64, 64);
    }

    private void addUI() {
        final Label quitInfoLabel = new Label("Press esc to return to main menu", game.skin);
        quitInfoLabel.setX(stage.getWidth() - quitInfoLabel.getWidth());
        quitInfoLabel.setY(stage.getHeight() - quitInfoLabel.getHeight());
        stage.addActor(quitInfoLabel);

        final Label pauseInfoLabel = new Label("Press space to pause the game", game.skin);
        pauseInfoLabel.setX(quitInfoLabel.getX());
        pauseInfoLabel.setY(quitInfoLabel.getY() - pauseInfoLabel.getHeight());
        stage.addActor(pauseInfoLabel);

        pointsLabel = new Label("Points: " + game.points, game.skin);
        pointsLabel.setX(0);
        pointsLabel.setY(stage.getHeight() - pointsLabel.getHeight());
        stage.addActor(pointsLabel);
    }

    private void updatePoints(boolean increase, float valueChange) {
        game.points += increase ? valueChange : -valueChange;
        pointsLabel.setText("Points: " + game.points);
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        handleInput();
        if (!pause) {
            shoot();
            updateNRemoveMovingObjects(delta);
            spawnEnemies();
        }

        game.batch.begin();

        drawObject(player, playerTexture);
        for (Enemy enemy : enemies) {
            drawObject(enemy, enemyTexture);
        }
        for (Bullet bullet : bullets) {
            drawObject(bullet, bulletTexture);
        }

        game.batch.end();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        removeCollided();
    }

    private void removeCollided() {
        for (Bullet bullet : bullets) {
            for (Enemy enemy : enemies) {
                if (enemy.getRectangle().overlaps(bullet.getRectangle())) {
                    float startingHealth = enemy.health;
                    enemy.decreaseHP(bullet.damage);
                    bullet.decreaseDamage(startingHealth);
                }
            }
        }
        if (enemies.size > 0) {
            Iterator<Enemy> enemyIterator = enemies.iterator();
            while (enemyIterator.hasNext()) {
                Enemy enemy = enemyIterator.next();
                if (enemy.dead) {
                    enemyIterator.remove();
                    updatePoints(true,1);
                } else {
                    if (enemy.getRectangle().overlaps(player.getRectangle())) {
                        dispose();
                        game.setScreen(new MainMenuScreen(game));
                    }
                }
            }
        }
        if (bullets.size > 0) {
            Iterator<Bullet> bulletIterator = bullets.iterator();
            while (bulletIterator.hasNext()) {
                if (bulletIterator.next().destroyed) {
                    bulletIterator.remove();
                }
            }
        }
    }

    private void spawnEnemies() {
        if (TimeUtils.nanoTime() - lastEnemyCreatedTime > enemySpawnDelay) {
            lastEnemyCreatedTime = TimeUtils.nanoTime();
            int x, y;
            if (random.nextBoolean()) {
                x = random.nextBoolean() ? 0 : game.defaultWidth;
                y = random.nextInt(game.defaultHeight);
            } else {
                x = random.nextInt(game.defaultWidth);
                y = random.nextBoolean() ? 0 : game.defaultHeight;
            }
            enemies.add(
                    new Enemy(
                            x,
                            y,
                            24,
                            24,
                            player.position.x,
                            player.position.y,
                            enemySpeed
                    )
            );
            enemySpeed *= enemySpeedMultiplier;
            enemySpawnDelay *= 1 - game.upgrades[game.ENEMIES_SPAWN_RATE_GROWTH].value;
        }
    }

    private void drawObject(GameObject gameObject, Texture texture) {
        Rectangle rectangle = gameObject.getRectangle();
        game.batch.draw(
                texture,
                rectangle.x,
                rectangle.y,
                rectangle.width,
                rectangle.height
        );
    }

    private void updateNRemoveMovingObjects(float delta) {
        for (Bullet bullet : bullets) {
            bullet.update(delta);
        }
        for (Enemy enemy : enemies) {
            enemy.update(delta);
        }

        if (bullets.size > 0) {
            Iterator<Bullet> bulletIterator = bullets.iterator();
            while (bulletIterator.hasNext()) {
                if (bulletIterator.next().outOfBounds(stage)) {
                    bulletIterator.remove();
                }
            }
        }
        if (enemies.size > 0) {
            Iterator<Enemy> enemyIterator = enemies.iterator();
            while (enemyIterator.hasNext()) {
                if (enemyIterator.next().outOfBounds(stage)) {
                    enemyIterator.remove();
                }
            }
        }
    }

    private void handleInput() {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.setScreen(new MainMenuScreen(game));
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            if (pause) {
                resume();
            }
            else {
                pause();
            }
        }
    }

    private void shoot(){
        if (Gdx.input.isTouched()) {
            Vector3 touchPosition = stage.getCamera().unproject(
                    new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)
            );
            if (TimeUtils.nanoTime() - lastBulletCreatedTime > game.upgrades[game.BULLET_DELAY].value) {
                lastBulletCreatedTime = TimeUtils.nanoTime();
                bullets.add(
                        new Bullet(
                                player.position.x,
                                player.position.y,
                                16,
                                16,
                                touchPosition.x,
                                touchPosition.y,
                                game.upgrades[game.BULLET_SPEED].value,
                                game.upgrades[game.BULLET_DAMAGE].value
                        )
                );
            }
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {
        pause = true;
    }

    @Override
    public void resume() {
        pause = false;
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        playerTexture.dispose();
        enemyTexture.dispose();
        bulletTexture.dispose();
    }

}

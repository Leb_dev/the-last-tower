package dev.cyberhatter.thelasttower;

class Enemy extends MovingGameObject {

    boolean dead = false;
    float health = 100f;

    Enemy(float x, float y, int width, int height, float toX, float toY, float speed) {
        super(x, y, width, height, toX, toY, speed);
    }

    void decreaseHP(float decrement) {
        if (decrement >= health) {
            dead = true;
        }
        else {
            health -= decrement;
        }
    }
}
